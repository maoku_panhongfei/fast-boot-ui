import service from '@/utils/request'

export const useArticleApi = (id: Number) => {
	return service.get('/site/article/' + id)
}

export const useArticleSubmitApi = (dataForm: any) => {
	if (dataForm.id) {
		return service.put('/site/article', dataForm)
	} else {
		return service.post('/site/article', dataForm)
	}
}
