import service from '@/utils/request'

export const useTagListApi = () => {
	return service.get('/site/tag/list')
}

export const useTagApi = (id: Number) => {
	return service.get('/site/tag/' + id)
}

export const useTagSubmitApi = (dataForm: any) => {
	if (dataForm.id) {
		return service.put('/site/tag', dataForm)
	} else {
		return service.post('/site/tag', dataForm)
	}
}

export const useTagDeleteApi = (id: Number) => {
	return service.delete('/site/tag/' + id)
}
