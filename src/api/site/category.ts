import service from '@/utils/request'

export const useCategoryListApi = () => {
	return service.get('/site/category/list')
}

export const useCategoryApi = (id: Number) => {
	return service.get('/site/category/' + id)
}

export const useCategorySubmitApi = (dataForm: any) => {
	if (dataForm.id) {
		return service.put('/site/category', dataForm)
	} else {
		return service.post('/site/category', dataForm)
	}
}
