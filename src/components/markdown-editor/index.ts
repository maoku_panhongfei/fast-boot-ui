import { withInstall } from '@/utils/tool'
import MarkdownEditor from './src/markdown-editor.vue'

export default withInstall(MarkdownEditor)
