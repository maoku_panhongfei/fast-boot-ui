import { withInstall } from '@/utils/tool'
import UserTableColumn from './src/user-table-column.vue'

export default withInstall(UserTableColumn)
